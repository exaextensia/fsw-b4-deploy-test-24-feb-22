const { Users } = require("../models");
const uuid = require("uuid");
const { token, encrypt } = require("../utils")

class UserController{

  static async login(req, res){
    const { name, password } = req.body
    const user = await Users.findOne({
      where: {
        name
      }
    })

    const compare = user ? encrypt.isPwdValid(password, user.dataValues.password) : 0
    console.log(user)

    if(!user || !compare){
      res.status(401).json({
        status: 401,
        message: "Unauthorize"
      })
      return
    }

    const access_token = token.makeToken(user.dataValues)
    res.status(200).json({ access_token })
  }

  static async getAllUser(req, res){
    try {
      const data = await Users.findAll()
      res.status(200).json({data})
    } catch (error) {
      console.log(error)
    }
  }

  static async getOneUser(req, res){
    const id = req.params.id
    const options = {
      where: {
        id
      },
    }
    const data = await Users.findOne(options)

    res.status(200).json({ data })
  }

  static async createUser(req, res){
    const id = uuid.v4()
    let { name, email, password } = req.body
    password = encrypt.encryptPwd(password)
    const payload = {
      id, name, email, password
    }

    const newUser = await Users.create(payload)
    res.status(200).json({data: newUser})
  }

  static async updateUser(req, res) {
    const { name, email, password } = req.body

    const payload = {
      name, email, password
    }

    const id = req.params.id
    const options = {
      where: {
        id
      },
      returning: true
    }

    const updatedUser = await Users.update(payload, options)

    res.status(200).json({data: updatedUser})
  }

  static async deleteUser(req, res){
    const id = req.params.id
    const deletedUser = await Users.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({ data: deletedUser })
  }
}

module.exports = UserController;