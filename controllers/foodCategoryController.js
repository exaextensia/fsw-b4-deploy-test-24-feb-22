const { FoodCategory } = require("../models")

class FoodCategoryController{
  static async getAllFoodCategory(req, res) {
    try {
      const data = await FoodCategory.findAll()
      res.status(200).json({ data })
    } catch (error) {
      console.log(error)
    }
  }

  static async getOneFoodCategory(req, res) {
    const id = req.params.id
    const options = {
      where: {
        id
      }
    }
    const data = await FoodCategory.findOne(options)

    res.status(200).json({ data })
  }
  
  static async createFoodCategory(req, res) {
    try {
      const { FoodsId, CategoryId } = req.body
      const payload = {
        FoodsId, CategoryId
      }
      console.log(payload)

      const newFoodCategory = await FoodCategory.create(payload)
      res.status(200).json({ data: newFoodCategory })
    } catch (error) {
      console.log(error)
    }

  }

  static async updateFoodCategory(req, res) {
    const { FoodsId, CategoryId } = req.body

    const payload = {
      FoodsId, CategoryId
    }
    const id = req.params.id
    const options = {
      where: {
        id
      },
      returning: true
    }
    const updatedFoodCategory = await FoodCategory.update(payload, options)

    res.status(200).json({
      data: updatedFoodCategory
    })
  }

  static async deleteFoodCategory(req, res) {
    const id = req.params.id
    const deletedFoodCategory = await FoodCategory.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({
      data: deletedFoodCategory
    })
  }
}

module.exports = FoodCategoryController