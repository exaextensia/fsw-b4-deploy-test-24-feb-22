const CategoryController = require("./categoryController");
const FoodCategoryController = require("./foodCategoryController");
const FoodController = require("./foodController");
const UserController = require("./userController");

module.exports = {
  UserController,
  FoodController,
  CategoryController,
  FoodCategoryController
}