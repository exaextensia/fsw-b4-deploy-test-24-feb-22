function userAuthorization(req, res, next){
  try {
    const payload = req.userLogin
    const id = req.params.id

    console.log(req.params)
    console.log(payload, "<<<<")
    console.log(id)

    const checkId = id ? payload.id === id : 0
    if(!checkId){
      res.status(401).json({
        msg: "No Access"
      })

      return
    }

    next()
  } catch (error) {
    console.log(error)
  }
}

module.exports = userAuthorization