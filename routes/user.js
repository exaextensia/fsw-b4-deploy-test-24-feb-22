const { UserController } = require("../controllers");
const route = require("express").Router();
const { authentication, userAuthorization } = require("../middleware")


route.get("/", UserController.getAllUser)
route.get("/:id", UserController.getOneUser)

route.post('/login', UserController.login)
route.post("/", UserController.createUser)

route.use(authentication)

route.put("/:id", userAuthorization, UserController.updateUser)
route.delete("/:id", userAuthorization, UserController.deleteUser)

module.exports = route