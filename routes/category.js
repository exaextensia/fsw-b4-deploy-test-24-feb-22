const { CategoryController } = require("../controllers");
const route = require("express").Router();

route.get("/", CategoryController.getAllCategory)
route.get("/:id", CategoryController.getOneCategory)
route.post("/", CategoryController.createCategory)
route.put("/:id", CategoryController.updateCategory)
route.delete("/:id", CategoryController.deleteCategory)

module.exports = route